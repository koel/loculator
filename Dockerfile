FROM ruby:2.7

RUN apt update && apt install -y default-libmysqlclient-dev lsof

WORKDIR /app
COPY . /app
RUN bundle install

ENV RAILS_ENV=production
ENV RAILS_SERVE_STATIC_FILES=true

RUN chmod +x ./entrypoint.sh bin/*
CMD ./entrypoint.sh
