class LocJob < ApplicationJob
  queue_as :loc_jobs # in-memory queue is fine for my purposes

  def perform(vcs_url, sha)
    @vcs_url = vcs_url
    @sha = sha

    ssh_path = Rails.application.config.cfg['ssh_key']

    final_out = {}
    # setup working dir
    Dir.mktmpdir 'loc' do |ws|
      puts "LocJob Temp dir: ", ws
      Dir.chdir(ws)
      # clone remote repo
      # setup command
      cmd = "GIT_SSH_COMMAND='ssh -i #{ssh_path} -o IdentitiesOnly=yes -o StrictHostKeyChecking=no' " \
        "git clone --depth 1 #{vcs_url} ./ 2>&1"
      cmd_output = `#{cmd}` # hectic backticks

      if cmd_output.include? 'not accessible: No such file or directory'
        final_out['error'] = 'Could not read private key: ' + ssh_path
      elsif cmd_output.include? 'Permission denied (publickey)'
        final_out['error'] = 'Failed to authenticate. SSH key mismatch.'
      elsif cmd_output.include? 'could not read Username'
        final_out['error'] = 'Repo does not exist, or is private.'
      elsif Dir.new(ws).children.reject { |name| name == '.git' }.empty? # dir empty
        final_out['error'] = 'Cloning failed for unknown reasons.'
      else
        # setup output
        final_out['total_lines'] = 0
        final_out['blank_lines'] = 0
        final_out['total_files'] = 0
        final_out['non_text_files'] = 0

        # do counting
        lines_in_dir ws, final_out
      end
    end

    if final_out['error']
      @error = final_out['error']
    else
      @results = final_out
    end
    finalise_cache

    # mark job complete
    cache_record = LocCache.find_by(vcs_url: @vcs_url)
    cache_record['running'] = false
    cache_record.save!

  end

  # Do DFS line counting
  private def lines_in_dir(dir, final_out)

    # iterate through all items in dir
    Dir.open(dir).each_child do |filename|
      full_file = dir + '/' + filename
      if File.directory? full_file
        lines_in_dir(full_file, final_out) unless filename == '.git' # ignore .git dir
      else
        if `file -ib #{full_file}`.start_with? 'text' # text file
          File.open(full_file) do |file|
            file.each do |line|
              final_out['total_lines'] += 1
              final_out['blank_lines'] += 1 if line.strip.empty?
            end
          end
        else
          final_out['non_text_files'] +=1
        end
        final_out['total_files'] += 1
      end
    end

  end

  # do the actual caching, if necessary
  private def finalise_cache
    # created during initial cache check
    cache = LocCache.find_by(vcs_url: @vcs_url)
    cache['latest_commit'] = @sha # will be nil for private non-specified
    cache['json_cache'] = @error ? { 'error': @error }.to_json : @results.to_json
    cache.save!
  end

end
