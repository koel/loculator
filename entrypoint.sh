#!/bin/bash

if [[ -f tmp/pids/server.pid ]]; then
  kill -9 $(lsof -i tcp:3000 -t)
  rm tmp/pids/server.pid
fi

# port 3000
exec bin/rails s -b 0.0.0.0
