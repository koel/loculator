# LOCulator web server

A pretty simple web server, serving as a wrapper for the counting app.

## Config

Config for this web app is in json format. The file should be specified in the environment variable `LOC_WEB_CFG` and set before running the app. Example config can be found in `example_cfg.json`.

## Running

First set up the db by running `schema.sql` like the animal you are.

Then run a dev server:

```sh
LOC_WEB_CFG=cfg.json bin/rails server
```

## Prod

Deploy this rail however you want.
